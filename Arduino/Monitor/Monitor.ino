#include <dht11.h>

dht11 DHT11;

#define DHT11PIN 2
#define DHT11PIN2 3
#define WLSPOWERPIN 4

#define LDRPIN A0
#define WLSPIN A1

boolean firstConnection = true;

void setup()
{
  Serial.begin(9600);
}

void loop()
{
  if(Serial)
    if(firstConnection)
    {
      Serial.print("Monitor\n");
      firstConnection = false;
    }
    else
    {
      Serial.print("<Monitor>");
      getTempHumid(DHT11PIN,"");
      getTempHumid(DHT11PIN2,"2");
      String output = getLDR();
      output += getWLS();
      output += "</Monitor>";
      output += '\n';
      Serial.println(output);  
    }
    delay(2000);
}

String getWLS()
{
  pinMode(WLSPOWERPIN,OUTPUT);
  digitalWrite(WLSPOWERPIN, HIGH);
  
  int reading = analogRead(WLSPIN);
  return "<WLS>" + String(reading) + "</WLS>";
  
  digitalWrite(WLSPOWERPIN, LOW);
  pinMode(WLSPOWERPIN,INPUT);
}

String getLDR()
{
  int LDRReading = analogRead(LDRPIN);
  return"<LDR>" + String(LDRReading) + "</LDR>";
}

double Fahrenheit(double celsius)
{
  return 1.8 * celsius + 32;
}

void getTempHumid(int pin, String n)
{
  int chk = DHT11.read(pin);
  switch(chk)
  {
    //case 0: Serial.println("OK"); break;
    //case -1: Serial.println("Checksum error"); break;
    //case -2: Serial.println("Time out error"); break;
    //default: Serial.println("Unknown error"); break;
  }
  
  float humid = (float)DHT11.humidity;
  float tempC = (float)DHT11.temperature;
  double tempF = Fahrenheit(DHT11.temperature);
  
  //Serial.print("Humidity (%): ");
  Serial.print("<Humid" + n + ">");
  Serial.print(humid, 2);
  Serial.print("</Humid" + n + ">");
 
  //Serial.print("Temperature (oC): ");
  Serial.print("<Temp" + n + "><C" + n + ">");
  Serial.print(tempC, 2);
  Serial.print("</C" + n + "><F" + n + ">");
 
  //Serial.print("Temperature (oF): ");
  Serial.print(tempF, 2);
  Serial.print("</F" + n + "></Temp" + n + ">");
}
