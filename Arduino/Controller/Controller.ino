#define FanPIN 2
#define PumpPIN 3
#define LightPIN 4

boolean firstConnection = true;

void setup()
{
  pinMode(FanPIN, OUTPUT);
  pinMode(PumpPIN, OUTPUT);
  pinMode(LightPIN, OUTPUT);
  swapDeviceState(7);
  
  Serial.begin(9600);
}

void loop()
{
  if(Serial)
    if(firstConnection)
    {
      Serial.print("Controller\n");
      firstConnection = false;
    }
    else if(Serial.available() > 0)
      {
        int readByte = Serial.read();//(Serial.read() - '0');
        Serial.println(readByte);
        swapDeviceState(readByte);
        delay(200);
        if(readByte >= 0 && readByte <= 4) 
		printDeviceStates();
      }
}

void swapDeviceState(int deviceNum)
{
  int state = 0;
  if(deviceNum == 0)
  {
      //ALL ON
      digitalWrite(LightPIN, LOW);
      digitalWrite(FanPIN, LOW);
      digitalWrite(PumpPIN, LOW);
  }
  else if(deviceNum == 1)
  {
      digitalWrite(LightPIN, LOW);
  }
  else if(deviceNum == 2)
  {
      digitalWrite(LightPIN, HIGH);
  }
  else if(deviceNum == 3)
  {
      digitalWrite(PumpPIN, LOW);
  }
  else if(deviceNum == 4)
  {
      digitalWrite(PumpPIN, HIGH);
  }
  else if(deviceNum == 5)
  {
      digitalWrite(FanPIN, LOW);
  }
  else if(deviceNum == 6)
  {
    digitalWrite(FanPIN, HIGH);
  }
  else if(deviceNum == 7)
  {
      //ALL OFF
      digitalWrite(LightPIN, HIGH);
      digitalWrite(FanPIN, HIGH);
      digitalWrite(PumpPIN, HIGH);
  }
}

void printDeviceStates()
{
  int lightState = digitalRead(LightPIN);
  int fanState = digitalRead(FanPIN);
  int pumpState = digitalRead(PumpPIN);
  
  Serial.print("<Controller><States><light>");
  Serial.print(lightState, 2);
  Serial.print("</light><fan>");
  Serial.print(fanState, 2);
  Serial.print("</fan><pump>");
  Serial.print(pumpState, 2);
  Serial.println("</pump></States></Controller>");
}
