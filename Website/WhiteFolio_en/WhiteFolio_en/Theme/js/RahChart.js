var pieLightOn = 0;
var lightState = 1;
var waterState = 1; 
var pumpTimes = new Array;
var waterLevel = new Array;
var waterLevelDates = new Array;
var temperature = new Array;
var humidity = new Array;
var cHumid;
var cTemp;

$( document ).ready(function() {
	Parse.initialize("k9UH8HfBh2kObyiosc1Pu99Sf3L3zEk7mgGLvo3S","InnSZEC8HqhQeA0A2LUjOOsJWxCeq8RheCWm7cjH");
	var LightEvent = Parse.Object.extend("Event");
	var query = new Parse.Query(LightEvent);
	query.equalTo("ScheduleId", "NUChTLicTS");
	query.descending("OnOffState");
	query.find({
	  success: function(results) {
	    // Do something with the returned Parse.Object values
	    
	    var dateOn = results[0].get('FirstOccurrence');
	    var dateOff = results[1].get('FirstOccurrence');
	    var onTime = dateOn.getTime();
	    var offTime = dateOff.getTime();
	    
	    var d = new Date();
	    if(d.getHours() > dateOn.getHours() && d.getHours() < dateOff.getHours()) lightState = 1;
	    else lightState = 0;
	    
	    pieLightOn = (offTime - onTime) / 1000 / 60 / 60; // mili->second->min->hour
	  },
	  error: function(error) {
	    alert("Error: " + error.code + " " + error.message);
	  }
	})
	.then(function(){
		getPumpData();
	});
});

function getPumpData()
{
	var monitorData = Parse.Object.extend("Event");
	var dataQuery = new Parse.Query(monitorData);
	dataQuery.equalTo("ScheduleId", "azc4J8lBGW");
	dataQuery.descending("OnOffState");
	dataQuery.find({
		success: function(results) {
			var dateOn = results[0].get('FirstOccurrence');
	    	var dateOff = results[1].get('FirstOccurrence');
			var onTime = dateOn.getTime();
	    	var offTime = dateOff.getTime();
	    	
	    	var difference = (offTime - onTime) / 1000 / 60;
	    	var interval = results[0].get('IntervalSeconds');
	    	interval = interval / 60;
	    	var repeat = 24 / (interval / 60);
	    	interval -= difference;
	    	for(i=0; i<repeat; i++)
	    	{
	    		pumpTimes.push(difference);
	    		pumpTimes.push(interval);
	    	}
	    	
	    	var currentDate = new Date();
	    	var hour = currentDate.getUTCHours()-4;
	    	var minute = currentDate.getUTCMinutes();
	    	hour %= 6;
	    	minute /= 16;
	    	if(hour == 0 && minute < 1) waterState = 1;
	    	else waterState = 2;
		},
		error: function(error) {
			alert("Error: " + error.code + " " + error.message);
		}
	})
	.then(function(){
		getMonitorData();
	});
}


function getMonitorData()
{
	var monitorData = Parse.Object.extend("MonitorData");
	var dataQuery = new Parse.Query(monitorData);
	dataQuery.descending("createdAt");
	dataQuery.limit(600);
	dataQuery.find({
		success: function(results) {
			
			cHumid = results[0].get("humidity");
			cTemp = results[0].get("fahrenheit");
			
			num = 0;
			average = 0;
			averageWL = 0;
			averageT = 0;
			averageH = 0;
			results.forEach(function(result)
			{
				var data = result.get('waterLevel');
				averageWL += data;
				
				var tData = result.get('fahrenheit');
				averageT += tData;
				
				var hData = result.get('humidity');
				averageH += hData;
				
				var date = result.createdAt;
				average += date.getTime();
				
				if(num == 29)
				{
					
					waterLevel.unshift((averageWL/30).toFixed(2));
					
					temperature.unshift((averageT/30).toFixed(2));
					
					humidity.unshift((averageH/30).toFixed(2));
					
					avgDate = new Date(average/30);
					waterLevelDates.unshift((avgDate.getMonth()+1) + "/" + avgDate.getDate() + "/" + avgDate.getFullYear());
					
					averageWL=0;
					averageT=0;
					averageH=0;
					average=0;
					num=-1;
				}		
				num++;
			});
		},
		error: function(error) {
			alert("Error: " + error.code + " " + error.message);
		}
	})
	.then(function(){
		checkAutomationStates();
	});
}

function checkAutomationStates()
{
	var automationState = Parse.Object.extend("AutomationState");
	var autoQuery= new Parse.Query(automationState);
	autoQuery.descending("createdAt");
	autoQuery.limit(2);
	autoQuery.find({
		success: function(results) {
			results.forEach(function(result)
			{
				var t = result.get("Type");
				switch(t)
				{
				case "Light":
					var ls = result.get("State");
					if(ls > 0)
					{
						ls %= 2;
						lightState = ls;
					}
					break;
				case "Pump":
					var ps = result.get("State");
					if(ps > 0)
					{
						ps %= 2;
						waterState = ps;
					}
					break;
				}
			});
		},
		error: function(error) {
			alert("Error: " + error.code + " " + error.message);
		}
	})
	.then(function(){
		initLighting();
		initWatering();
		initTempHumid();
	});
}

function initLighting() {
	//24hour chart
	var ctx = $("#lightChart").get(0).getContext("2d");
    var on = Math.round(pieLightOn);
    var off = Math.round(24 - pieLightOn);
    var lightdata = [
	    {
	        value: on,
	        color:"#F7464A",
	        highlight: "#FF5A5E",
	        label: "Light On"
	    },
	    {
	        value: off,
	        color: "#46BFBD",
	        highlight: "#5AD3D1",
	        label: "Light Off"
	    }
	];
	var myNewChart = new Chart(ctx).Doughnut(lightdata);
	
	var lightImage = document.getElementById("lightState");
	var lightLabel = document.getElementById("lightStateLabel");
	if(lightState == 0)
	{
		lightImage.src = "images/lightoff.jpg";
		lightLabel.textContent = "Off";
	}
	else if(lightState == 1)
	{
		lightImage.src = "images/lighton.jpg";
		lightLabel.textContent = "On";
	}
};

function initWatering() {
	
	var ctxWater = $("#waterChart").get(0).getContext("2d");
	
	var waterData = new Array;
	
	for(i=0; i<pumpTimes.length;i+=2)
	{
		var onObject = {
			"value": pumpTimes[i],
			"color": "#46BFBD",
			"highlight": "#5AD3D1",
	        "label": "Pump On"
		};
		var offObject = {
			"value": pumpTimes[i+1],
			"color": "#FDB45C",
			"highlight": "#FFC870",
	        "label": "Pump Off"
		};
		waterData.push(onObject);
		waterData.push(offObject);
	}
    var waterChart = new Chart(ctxWater).Pie(waterData);
    
	var pumpImage = document.getElementById("pumpState");
	var pumpLabel = document.getElementById("pumpStateLabel");
    
    if(waterState == 0)
	{
		pumpImage.src = "images/pumpoff.png";
		pumpLabel.textContent = "Off";
	}
	else if(waterState == 1)
	{
		pumpImage.src = "images/pumpon.png";
		pumpLabel.textContent = "On";
	}
    
    //Water Level
    var ctx2 = $("#waterLevelChart").get(0).getContext("2d");
	var data = {
		labels: waterLevelDates,
            datasets: [
                {
                    fillColor: "rgba(151,187,205,0.5)",
                    strokeColor: "rgba(151,187,205,1)",
                    pointColor: "rgba(151,187,205,1)",
                    pointStrokeColor: "#fff",
                    data: waterLevel
                },
                //{
                //    fillColor: "rgba(151,187,205,0.5)",
                //    strokeColor: "rgba(151,187,205,1)",
                //    pointColor: "rgba(151,187,205,1)",
                //    pointStrokeColor: "#fff",
                //    data: [28, 48, 40, 19, 96, 27, 100]
               // }
            ]
        };
        ctx2.canvas.width = 850;

    var myNewChart = new Chart(ctx2).Line(data);

}

function initTempHumid()
{
	var temp = $("#tempChart").get(0).getContext("2d");
	
	var data = {
		labels: waterLevelDates,
            datasets: [
                {
                    fillColor: "rgba(151,187,205,0.5)",
                    strokeColor: "rgba(151,187,205,1)",
                    pointColor: "rgba(151,187,205,1)",
                    pointStrokeColor: "#fff",
                    data: temperature
                }
            ]
        };
        temp.canvas.width = 850;
	
	var tHChart = new Chart(temp).Line(data);
	
	///Humidity
	
	var humid = $("#humidChart").get(0).getContext("2d");
	
	var data = {
		labels: waterLevelDates,
            datasets: [
                {
                    fillColor: "rgba(151,187,205,0.5)",
                    strokeColor: "rgba(151,187,205,1)",
                    pointColor: "rgba(151,187,205,1)",
                    pointStrokeColor: "#fff",
                    data: humidity
                }
            ]
        };
        humid.canvas.width = 850;
	
	var tHChart = new Chart(humid).Line(data);
	
	///Curent Values
	
	var currentT = document.getElementById("currentTemp");
	var currentH = document.getElementById("currentHumid");
	
	currentT.textContent = cTemp + "\u00B0";
	currentH.textContent = cHumid + "%";
}
             