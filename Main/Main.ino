#include <dht11.h>

dht11 DHT11;

#define DHT11PIN 2
#define LDRPIN A0
#define WLSPIN 4

void setup()
{
  Serial.begin(9600);
  pinMode(WLSPIN, INPUT);
  digitalWrite(WLSPIN, HIGH);
}

void loop()
{
  getTempHumid();
  getLDR();
  getWLS();
 
 
  delay(2000);
}

void getWLS()
{
  int reading = digitalRead(WLSPIN);
  Serial.print("Water Level: ");
  Serial.println(reading);
}

void getLDR()
{
  int LDRReading = analogRead(LDRPIN);
  Serial.print("Light Level: ");
  Serial.println(LDRReading);
}

double Fahrenheit(double celsius)
{
  return 1.8 * celsius + 32;
}

void getTempHumid()
{
  int chk = DHT11.read(DHT11PIN);
  switch(chk)
  {
    case 0: Serial.println("OK"); break;
    case -1: Serial.println("Checksum error"); break;
    case -2: Serial.println("Time out error"); break;
    default: Serial.println("Unknown error"); break;
  }
  
  Serial.print("Humidity (%): ");
  Serial.println((float)DHT11.humidity, 2);
 
  Serial.print("Temperature (oC): ");
  Serial.println((float)DHT11.temperature, 2);
 
  Serial.print("Temperature (oF): ");
  Serial.println(Fahrenheit(DHT11.temperature), 2);
}
