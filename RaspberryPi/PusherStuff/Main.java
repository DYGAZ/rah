import java.util.Timer;
import java.util.TimerTask;

import com.pusher.client.Pusher;
import com.pusher.client.channel.Channel;
import com.pusher.client.channel.SubscriptionEventListener;
import com.pusher.client.connection.ConnectionEventListener;
import com.pusher.client.connection.ConnectionState;
import com.pusher.client.connection.ConnectionStateChange;


public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Timer t = new Timer();
		Pusher pusher = new Pusher("76e110dc07d913d37001");
		
		pusher.connect(new ConnectionEventListener() {
			
			@Override
			public void onError(String arg0, String arg1, Exception arg2) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onConnectionStateChange(ConnectionStateChange arg0) {
				// TODO Auto-generated method stub
				
			}
		}, ConnectionState.ALL);
		
		Channel channel = pusher.subscribe("event_update_channel");
		
		channel.bind("event_updated", new SubscriptionEventListener() {
			
			@Override
			public void onEvent(String channel, String event, String data) {
				System.out.println("Event received with data: " + data);
			}
		});
		
		t.scheduleAtFixedRate(new TimerTask() {
			
			@Override
			public void run() {
				System.out.println("Waiting...");
			}
		}, 0, 5000);

	}

}
