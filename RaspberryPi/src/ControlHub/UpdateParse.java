package ControlHub;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TimerTask;

public class UpdateParse extends TimerTask{

	static SerialManager monitorManager = new SerialManager();
	static ParseServerCommunicator parseCommunicator = new ParseServerCommunicator();
	
	public UpdateParse(SerialManager mManager, ParseServerCommunicator pCommunicator)
	{
		monitorManager = mManager;
		parseCommunicator = pCommunicator;
	}
	
	@Override
	public void run() {
		System.out.println("********************************************Pushing MonitorData*********************************************");
		System.out.println("Querying Monitors");
		Map<String, MonitorData> mData = queryMonitors();
		
		System.out.println("Pushing data to Parse");
		if(mData.size() > 0)
			pushMonitorDataToParse(mData);
		
	}
	
	public static Map<String, MonitorData> queryMonitors()
	{
		Map<String, MonitorData> dataList = new HashMap<String, MonitorData>();
		
		Set<String> portNames = monitorManager.getKeys();
		for(String portName: portNames)
		{
			System.out.println(portName);
			
			for(int i=0;i<3;i++) //try 3 times to get xml
			{
				String xml = monitorManager.GetXMLFromSerial(portName);
				System.out.println(xml);
				MonitorData data = new MonitorData(xml);
				if(data.ParseSuccess)
				{
					data.printDataList();
					dataList.put(portName, data);
					i=3;
					
				}
			}
		}
		
		return dataList;
		
	}
	
	public static void pushMonitorDataToParse(Map<String, MonitorData> data)
	{
		Set<String> keys = data.keySet();
		for(String portName: keys)
		{
			System.out.println(portName);
			MonitorData mData = data.get(portName);
			if(mData != null)
			{
				mData.put("PortName", portName);
				System.out.println("Setting up data to push to Parse...");
				if(mData.ParseSuccess)
					parseCommunicator.WriteMonitorData(mData);
			}
		}
	}
	

}
