package ControlHub;

import java.util.*;

public class SerialManager {
	
	HashMap<String, SerialCommunicator> serialList = new HashMap<String, SerialCommunicator>();
	
	public void buildSerialList(List<String> portNames)
	{
		for(String port: portNames)
		{
			SerialCommunicator communicator = new SerialCommunicator();
			communicator.searchForPorts();
			communicator.connect(port);
			if(communicator.Connected){
				if(communicator.initIOStream()){
					communicator.initListener();
				}
			}
			
			serialList.put(port, communicator);
		}
	}
	
	public Set<String> getKeys()
	{
		Set<String> keys =serialList.keySet();
		return keys;
	}
	
	public String GetXMLFromSerial(String portName)
	{
		String xml = readDataFromSerial(portName);
		return xml;
	}
	
	private String readDataFromSerial(String portName)
	{
		//Write message to serial in order to query arduino
		//Wait for event listener to return a response (maybe add a timeout)
		//Response should be xml, send it on its way
		//
		SerialCommunicator currentCommunicator = serialList.get(portName);
		String xml = "";
		
		try 
		{
			Thread.sleep(3000);
			xml = currentCommunicator.LastData;
		} catch (InterruptedException e) 
		{
			e.printStackTrace();
		}
		
		return xml;
	}

	public void writeToController(String portName, byte value)
	{
		SerialCommunicator controller = serialList.get(portName);
		System.out.println("Writing to PortName:" + portName);
		controller.writeToSerial(value);
	}
}
