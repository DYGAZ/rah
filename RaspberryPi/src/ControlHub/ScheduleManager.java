package ControlHub;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.Timer;

import almonds.ParseObject;

public class ScheduleManager {
	
	ParseServerCommunicator pCommunicator;
	SerialManager cManager;
	Timer scheduleTimer;
	
	public ScheduleManager(ParseServerCommunicator pcommunicator, SerialManager controlManager)
	{
		pCommunicator = pcommunicator;
		cManager = controlManager;
		scheduleTimer = new Timer();
		updateSchedule();
	}
	
	public void updateSchedule()
	{
		scheduleTimer.cancel();
		scheduleTimer = new Timer();
		System.out.println("*********************************************UPDATING SCHEDULES*********************************************");
		updateLightSchedule();
		updatePumpSchedule();
	}
	
	void updateLightSchedule()
	{
		
		List<ParseObject> lightSchedules = pCommunicator.QuerySchedule(1);
		for(ParseObject schedule: lightSchedules)
		{
			//Need to check override state on schedule an call some write function on cManager to utilize the override then disregard the steps below
			int scheduleOverride = (Integer) schedule.get("OverrideState");
			Set<String> keys = cManager.serialList.keySet();
			if(keys.size() > 0)
			{
				String portName = (String) keys.toArray()[0];
				
				switch(scheduleOverride)
				{
				case 0://No override continue with scheduling
					pCommunicator.WritePumpLightState(0, "Light");
					System.out.println("\nScheduling Light Task");
					String id = schedule.getObjectId();
					List<ParseObject> events = getScheduleEvents(id);
					events = orderEvents(events);
					for(ParseObject event: events)
					{
						
						int state = (Integer) event.get("OnOffState");//State of 1 is On, State of 0 is Off
						System.out.println("\nScheduling Light Event...");
						scheduleEvent(event, (2-state));	//1 is On, 2 is Off.
					}
					break;
				case 1://Override to On state
					pCommunicator.WritePumpLightState(1, "Light");
					System.out.println("Override from User: Turn Light On");
					cManager.writeToController(portName, (byte)1);
					cManager.writeToController(portName, (byte)5);
					break;
				case 2://Ovveride to Off state
					pCommunicator.WritePumpLightState(2, "Light");
					System.out.println("Override from User: Turn Light Off");
					cManager.writeToController(portName, (byte)2);
					cManager.writeToController(portName, (byte)6);
					break;
				}
			}
		}
	}
	
	void updatePumpSchedule()
	{
		List<ParseObject> pumpSchedules = pCommunicator.QuerySchedule(2);
		for(ParseObject schedule: pumpSchedules)
		{
			int scheduleOverride = (Integer) schedule.get("OverrideState");
			Set<String> keys = cManager.serialList.keySet();
			if(keys.size() > 0)
			{
				String portName = (String) keys.toArray()[0];
			
				switch(scheduleOverride)
				{
				case 0:
					pCommunicator.WritePumpLightState(0, "Pump");
					System.out.println("\nScheduling Pump Task");
					String id = schedule.getObjectId();
					List<ParseObject> events = getScheduleEvents(id);
					events = orderEvents(events);
					for(ParseObject event: events)
					{
						int state = (Integer) event.get("OnOffState");//State of 1 is On, State of 0 is Off
						System.out.println("\nScheduling Pump Event...");
						scheduleEvent(event, (4-state));	//3 is On, 4 is Off.
					}
					break;
				case 1://override to On state
					pCommunicator.WritePumpLightState(1, "Pump");
					System.out.println("Override from User: Turn Pump On");
					cManager.writeToController(portName, (byte)3);
					break;
				case 2://override to Off state
					pCommunicator.WritePumpLightState(2, "Pump");
					System.out.println("Override from User: Turn Pump Off");
					cManager.writeToController(portName, (byte)4);
					break;
				}
			}
			
			
		}
	}
	
	List<ParseObject> getScheduleEvents(String scheduleId)
	{
		List<ParseObject> events = pCommunicator.QueryEvents(scheduleId);
		return events;
	}
	
	List<ParseObject> orderEvents(List<ParseObject> events)
	{	
		//This will only work in the case of two events
		//Some sorting algorithm is required to order a List large than 2
		//Insertion sort although inefficient should work fine here
		//
		System.out.println("\nORDERING EVENTS:");
		for(ParseObject event: events)
		{
			System.out.println("Event: " + event.get("FirstOccurrence") + " Interval: " + event.get("IntervalSeconds"));
		}

		Calendar e = parseParseDate(events.get(0).get("FirstOccurrence").toString());
		long event1 = getEventTime(e, 1000 * Long.parseLong(events.get(0).get("IntervalSeconds").toString()));
		
		Calendar minCal = parseParseDate(events.get(1).get("FirstOccurrence").toString());
		long event2 = getEventTime(minCal, 1000 * Long.parseLong(events.get(1).get("IntervalSeconds").toString()));

		System.out.println("Event1: " + event1);
		System.out.println("Event2: " + event2);
		
		//if both events are less than time execute smallest first
		//if one event is greater than current execute first
		//if both events are greater than current execute largest
		
		Calendar c = Calendar.getInstance();
		long currentHour = getTimeinHour(c);
		
		boolean swap = false;
		
		///Before swapping [e1][e2]
		
		if(event1 < currentHour && event2 < currentHour)		////[e1,e2][c]
		{
			if(event1 > event2) swap = true;					////[e2][e1][c] ->requires swap to get e2(lesser) first
		}
		else if(event1 > currentHour && event2 > currentHour)	////[c][e1,e2]
		{
			if(event1 > event2)swap = true;						////[c][e2][e1] ->requires swap to get e1(greater) second
		}
		else if(event1 < currentHour && event2 > currentHour)	////[e1][c][e2]
		{
			swap = true;//requires swap so that e1(last occurring) will occur last
		}
		else if(event1 > currentHour && event2 < currentHour)////[e2][c][e1]
		{
			//no swap because e2 should happen last and it already does
		}
		
		if(swap)
		{
			System.out.println("Swapping");
			ParseObject a = events.get(0);
			ParseObject b = events.get(1);
			events.set(0, b);
			events.set(1, a);
		}
		
		for(ParseObject event: events)
		{
			System.out.println("Event: " + event.get("FirstOccurrence") + " Interval: " + event.get("IntervalSeconds"));
		}
		return events;
	}
	
	long getEventTime(Calendar c, long interval)
	{
		long eTime = getTimeinHour(c);
		long current = getTimeinHour(Calendar.getInstance());
		if(current > eTime)
		{
			System.out.println("Current: " + current + "\nEventTime: " + eTime);
			long diff = current - eTime;
			long timeSinceLast = diff % interval;
			System.out.println("Diff: " + diff + "\nTimeFromLast: " + timeSinceLast + "\nInterval: " + interval);
			diff -= timeSinceLast;
			eTime += diff;
			System.out.println("EventTime: " + eTime);
		}
		
		return eTime;
	}
	
	long getTimeinHour(Calendar c)
	{
		long eTime = c.get(Calendar.HOUR_OF_DAY) * 60 * 60 * 1000;
		eTime += c.get(Calendar.MINUTE) * 60 * 1000;
		eTime += c.get(Calendar.SECOND) * 1000;
		eTime += c.get(Calendar.MILLISECOND);
		
		return eTime;
	}

	
	void scheduleEvent(ParseObject event, int commandType)
	{
		//NEED to get rid of date object and use calendar
		Object d = event.get("FirstOccurrence");
		Calendar firstOccurance = parseParseDate(d.toString());
		long intervalSeconds = Long.parseLong(event.get("IntervalSeconds").toString());
		intervalSeconds *= 1000;	//Convert seconds to milliseconds
		
		Date scheduleDate = getScheduleDate(firstOccurance, intervalSeconds);
		
		System.out.println("ScheduledDate: " + scheduleDate + " Interval mls: " + intervalSeconds);
		scheduleTimer.scheduleAtFixedRate(new AutomationTask(cManager, commandType), scheduleDate, intervalSeconds);
	}
	
	Calendar parseParseDate(String parseDate)
	{
		//{"__type":"Date","iso":"2014-07-26T04:00:00.000Z:}
		int year = Integer.parseInt(parseDate.substring(24, 28));
		int month = Integer.parseInt(parseDate.substring(29, 31));
		int day = Integer.parseInt(parseDate.substring(32, 34));
		int hour = Integer.parseInt(parseDate.substring(35, 37));
		int minute = Integer.parseInt(parseDate.substring(38, 40));
		int second = Integer.parseInt(parseDate.substring(41, 43));
		
		Calendar c = Calendar.getInstance();
		c.set(Calendar.YEAR, year);
		c.set(Calendar.MONTH, month-1);
		c.set(Calendar.DAY_OF_MONTH, day);
		c.set(Calendar.HOUR_OF_DAY, hour);
		c.set(Calendar.MINUTE, minute);
		c.set(Calendar.SECOND, second);
		return c;
	}

	Date getScheduleDate(Calendar c, long interval)
	{
		System.out.println("Scheduled Calendar Value: " + c.getTime());
		long scheduledAsInt = c.getTimeInMillis();
		scheduledAsInt = getLatestEvent(scheduledAsInt, interval);
		
		Date d = new Date(scheduledAsInt);
		return d;
	}
	
	long getLatestEvent(long scheduled, long interval)
	{
		long current = Calendar.getInstance().getTimeInMillis();
		if(current > scheduled)
		{
			long difference = current - scheduled;
			long timeLast = difference % interval;
			difference -= timeLast;
			scheduled += difference;
		}
		return scheduled;
	}
}
