package ControlHub;

import gnu.io.*;
import java.io.*;
import java.util.*;


/*
 * This class is setup for a single serial connection
 * each serial device connected will require its own instance of this class
 * 
 */

public class SerialCommunicator implements SerialPortEventListener{

    //for containing the ports that will be found
    private Enumeration<CommPortIdentifier> ports = null;
    //map the port names to CommPortIdentifiers
    protected HashMap<String, CommPortIdentifier> portMap = new HashMap<String, CommPortIdentifier>();
    
    public boolean Connected = false;
    String readData = "";
    String LastData = "";

    //this is the object that contains the opened port
    protected CommPortIdentifier selectedPortIdentifier = null;
    protected SerialPort serialPort = null;

    //input and output streams for sending and receiving data
    protected InputStream input = null;
    private OutputStream output = null;

    //the timeout value for connecting with the port
    final static int TIMEOUT = 2000;

    //some ASCII values for for certain things
    final static int SPACE_ASCII = 32;
    final static int DASH_ASCII = 45;
    final static int NEW_LINE_ASCII = 10;
    
	@Override
	public void serialEvent(SerialPortEvent evt) {
		if (evt.getEventType() == SerialPortEvent.DATA_AVAILABLE)
        {
            try
            {
                byte singleData = (byte)input.read();

                if (singleData != NEW_LINE_ASCII)
                {
                	readData += new String(new byte[] {singleData});
                    //System.out.println(new String(new byte[] {singleData}));
                }
                else
                {
                	if(!readData.equals("\r"))
                		LastData = readData;
                	readData = "";  
                }
            }
            catch (Exception e)
            {
            	 System.out.println("Failed to read data. (" + e.toString() + ")");
            }
        }
		
	}

    public void searchForPorts()
    {
        ports = CommPortIdentifier.getPortIdentifiers();

        while (ports.hasMoreElements())
        {
            CommPortIdentifier curPort = (CommPortIdentifier)ports.nextElement();

            //get only serial ports
            if (curPort.getPortType() == CommPortIdentifier.PORT_SERIAL)
            {
                portMap.put(curPort.getName(), curPort);
            }
        }
    }
    
    public void connect(String port)
    {
        String selectedPort = port;
        selectedPortIdentifier = (CommPortIdentifier)portMap.get(selectedPort);

        CommPort commPort = null;

        try
        {
            //the method below returns an object of type CommPort
            commPort = selectedPortIdentifier.open("TigerControlPanel", TIMEOUT);
            //the CommPort object can be casted to a SerialPort object
            serialPort = (SerialPort)commPort;
            Connected = true;
        }
        catch (PortInUseException e)
        {
        	System.out.println(e.toString());
        }
        catch (Exception e)
        {
        	System.out.println(e.toString());
        }
    }
    
    public boolean initIOStream()
    {
        //return value for whether opening the streams is successful or not
        boolean successful = false;

        try {
            //
            input = serialPort.getInputStream();
            output = serialPort.getOutputStream();
            writeData(0, 0);

            successful = true;
            return successful;
        }
        catch (IOException e) {
            System.out.println("I/O Streams failed to open. (" + e.toString() + ")");
            return successful;
        }
    }
    
    public void initListener()
    {
        try
        {
            serialPort.addEventListener(this);
            serialPort.notifyOnDataAvailable(true);
        }
        catch (TooManyListenersException e)
        {
        	System.out.println("Too many listeners. (" + e.toString() + ")");
        }
    }
    
    public void disconnect()
    {
        //close the serial port
        try
        {
            writeData(0, 0);

            serialPort.removeEventListener();
            serialPort.close();
            input.close();
            output.close();

            System.out.println("Disconnected");
            Connected = false;
            readData = "";
        }
        catch (Exception e)
        {
        	System.out.println("Failed to close " + serialPort.getName()
                              + "(" + e.toString() + ")");
        }
    }
    
    public void writeData(int leftThrottle, int rightThrottle)
    {
        try
        {
            output.write(leftThrottle);
            output.flush();
            //this is a delimiter for the data
            output.write(DASH_ASCII);
            output.flush();

            output.write(rightThrottle);
            output.flush();
            //will be read as a byte so it is a space key
            output.write(SPACE_ASCII);
            output.flush();
        }
        catch (Exception e)
        {
            System.out.println("Failed to write data. (" + e.toString() + ")");
        }
    }
    
    public void writeToSerial(byte value)
    {
    	try
        {
    		System.out.println("Writing: " + value + " to Serial");
            output.write(value);
            output.flush();
        }
        catch (Exception e)
        {
            System.out.println("Failed to write data. (" + e.toString() + ")");
        }
    }

    public void printAvailablePorts()
    {
    	Set<String> keys = portMap.keySet();
    	for(String key: keys)
    		System.out.println(key);
    }
}
