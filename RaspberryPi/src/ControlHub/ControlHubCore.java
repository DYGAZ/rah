package ControlHub;

import java.util.*;

//To AutoRun on Raspbian
// sudo nano /etc/rc.local <- edit this file
public class ControlHubCore {
	
	static SerialManager monitorManager = new SerialManager();
	static SerialManager controllerManager = new SerialManager();
	static ParseServerCommunicator parseCommunicator = new ParseServerCommunicator();
	static PusherManager pManager;
	static ScheduleManager sManager;
	static boolean shutdown = false;
	static String ControlHubId = "MffI2vaIFL"; //Must exist in Database first
	

	
	public static void main(String[] args) throws Exception
	{

		//TestCaseForControlHub tester = new TestCaseForControlHub();
		//tester.RunTest();
		
		//Should be 15 minutes eventually
		final int UpdateInterval = 900000;	//15 minutes
		
		Date current = new Date();
		System.out.println("Current Date: " + current.toString());
		
		//final int UpdateInterval = 5000;
		initializeControlHub();
		
		Timer timer = new Timer();
		TimerTask writeParse =  new UpdateParse(monitorManager, parseCommunicator);
		timer.scheduleAtFixedRate(writeParse, 0, UpdateInterval);

	}
	
	public static void initializeControlHub() throws InterruptedException
	{
		sManager = new ScheduleManager(parseCommunicator, controllerManager);
		pManager = new PusherManager(sManager);
		setupSerialToArduino();
		sManager.updateSchedule();
	}
	
	public static void setupSerialToArduino() throws InterruptedException
	{
		System.out.println("Locating Arduino Serial Port");
		ArduinoSerialLocator serialLocator = new ArduinoSerialLocator();
		
		System.out.println("Available Serial Ports:");
		serialLocator.searchForPorts();
		serialLocator.printAvailablePorts();
		
		serialLocator.checkMonitorController();
		System.out.println("Available Arduino Serial Ports:");
		serialLocator.printList();
		
		System.out.println("Setting up Serial Manager with Arduino Ports...");
		List<String> monitorList = serialLocator.getMonitorList();
		List<String> controllerList = serialLocator.getControllerList();
		
		monitorManager.buildSerialList(monitorList);
		controllerManager.buildSerialList(controllerList);
		
		System.out.println("Writing Monitor and Controller to Parse");
		WritePortToParse(monitorList, "Monitor");
		WritePortToParse(controllerList, "Controller");
		
		System.out.println("Setup Complete.");
	}
	
	public static void WritePortToParse(List<String> ports, String type)
	{
		parseCommunicator.WriteSerialPort(ports, type, ControlHubId);
	}
}
