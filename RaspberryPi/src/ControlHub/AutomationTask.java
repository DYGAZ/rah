package ControlHub;

import java.util.Date;
import java.util.Set;
import java.util.TimerTask;

public class AutomationTask extends TimerTask{
	
	SerialManager cManager;
	int command;
	
	public AutomationTask(SerialManager controlManager, int commandType) //Command should be 0-all on, 1,2,3,4,5,6, 7-all off
	{
		cManager = controlManager;
		command  = commandType;
	}

	@Override
	public void run() {
		System.out.println("Running AutomationTask");
		long date = this.scheduledExecutionTime();
		Date d = new Date(date);
		System.out.println(d.toString());
		Set<String> keys = cManager.serialList.keySet();
		String portName = (String) keys.toArray()[0];
		
		cManager.writeToController(portName, (byte)command);
		if(command == 1) cManager.writeToController(portName, (byte)5);
		else if(command == 2) cManager.writeToController(portName, (byte)6);
	}
}
