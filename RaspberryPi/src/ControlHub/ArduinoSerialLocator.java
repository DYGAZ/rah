package ControlHub;

import java.util.Iterator;
import java.util.Map;
import java.util.*;

import gnu.io.*;

public class ArduinoSerialLocator extends SerialCommunicator{
    
    List<String> monitorList = new ArrayList<String>();
    List<String> controllerList = new ArrayList<String>();
	
    //Override the SerialEvent and listen for Controller or Monitor
    
	public void serialEvent(SerialPortEvent evt) {
		
		if (evt.getEventType() == SerialPortEvent.DATA_AVAILABLE)
        {
            try
            {
                byte singleData = (byte)input.read();

                if (singleData != NEW_LINE_ASCII)
                {
                	readData += new String(new byte[] {singleData});
                    System.out.println("Current Read Data: " + readData.toString());
                }
                else{
                	System.out.println("Device Found: " + readData.toString());
                	String arduinoType = readData.toString();
                	if(arduinoType.equals("Monitor"))
                	{
                		System.out.println("Adding Monitor...");
            			String name = selectedPortIdentifier.getName();
            			if(!monitorList.contains(name))
            				System.out.println(name + " Added!");
            				monitorList.add(name);
                	}
                	else if(arduinoType.equals("Controller"))
                	{
                		System.out.println("Adding Controller...");
                		String name = selectedPortIdentifier.getName();
                		if(!controllerList.contains(name))
                			System.out.println(name + " Added!");
                			controllerList.add(name);
                	}
                	readData = "";  
                }
            }
            catch (Exception e)
            {
            	 System.out.println("Failed to read data. (" + e.toString() + ")");
            }
        }
	}
   
    public void checkMonitorController() throws InterruptedException
    {
    	Iterator it = portMap.entrySet().iterator();
    	while(it.hasNext())
    	{
    		Map.Entry pair = (Map.Entry)it.next();
    		
    		connect((String)pair.getKey());
    		System.out.println("Connecting to: " + (String)pair.getKey());
    		if(Connected)
    		{
    			System.out.println("Connected");
    			if(initIOStream())
    			{
    				System.out.println("Starting Listener");
    				initListener();
    			}
    		}
    		System.out.println("Waiting 2 seconds");
	    	Thread.sleep(2000);
        	disconnect();
    	}
    }
    
    public List<String> getMonitorList()
    {
    	return monitorList;
    }
    
    public List<String> getControllerList()
    {
    	return controllerList;
    }
    
    public void printList()
    {
    	System.out.println("Monitor:");
    	for(String monitor: monitorList)
    		System.out.println(monitor);
    	
    	System.out.println("Controller:");
    	for(String controller: controllerList)
    		System.out.println(controller);
    }
}
