package ControlHub;

import java.io.StringReader;
import java.util.*;
import javax.xml.parsers.*;

public class MonitorData {
	
	private Map<String, Object> dataList = new HashMap<String, Object>();
	public boolean ParseSuccess = true;
	
	public MonitorData(String xml)
	{
		float humid = Float.parseFloat(getElementValue("Humid", xml));
		float humid2 = Float.parseFloat(getElementValue("Humid2", xml));
				
		//float celsius = Float.parseFloat(getValue("celsius", element));
				
		float fahrenheit = Float.parseFloat(getElementValue("F", xml));
		float fahrenheit2 = Float.parseFloat(getElementValue("F2", xml));
		
		int ldr = Integer.parseInt(getElementValue("LDR", xml));
		int wls = Integer.parseInt(getElementValue("WLS", xml));
		
		if(ParseSuccess)
		{
			dataList.put("humidity", (humid + humid2)/2);
			//dataList.put("celsius",celsius);
			dataList.put("fahrenheit", (fahrenheit + fahrenheit2)/2);
			dataList.put("LDR", ldr);
			dataList.put("waterLevel", wls);
		}
				
	}
	
	String getElementValue(String tagName, String xml)
	{
		String tag = "";
		char[] data = xml.toCharArray();
		
		for(int i=0; i<data.length; i++)
		{
			if(data[i] == '<') continue;
			else if(data[i] == '>')
			{
				if(tag.equals(tagName))
				{
					String returnValue = "";
					char c = data[i + 1];
					while(c != '<')
					{
						returnValue += c;
						i++;
						c = data[i+1];
					}
					return returnValue;
				}	
				tag = "";
				continue;
			}
			tag += data[i];
			
		}
		ParseSuccess = false;
		return "0";
	}
	
	public void printDataList()
	{
		Iterator it = dataList.entrySet().iterator();
		while(it.hasNext())
		{
			Map.Entry pairs = (Map.Entry)it.next();
	        System.out.println(pairs.getKey() + " = " + pairs.getValue());
		}
	}
	
	public Set<String> GetParamNames()
	{
		Set<String> names = dataList.keySet();
		return names;
	}
	
	public Object get(String key)
	{
		return dataList.get(key);
	}
	
	public void put(String key, Object value)
	{
		dataList.put(key, value);
	}
}
