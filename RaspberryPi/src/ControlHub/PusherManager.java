package ControlHub;

import com.pusher.client.Pusher;
import com.pusher.client.channel.Channel;
import com.pusher.client.channel.SubscriptionEventListener;
import com.pusher.client.connection.ConnectionEventListener;
import com.pusher.client.connection.ConnectionState;
import com.pusher.client.connection.ConnectionStateChange;

public class PusherManager {
	
	ScheduleManager sManager;
	
	public PusherManager(ScheduleManager smanager)
	{
		sManager = smanager;
		
		System.out.println("Setting up connection to Pusher");
		Pusher pusher = new Pusher("76e110dc07d913d37001");
		
		pusher.connect(new ConnectionEventListener() {
			
			@Override
			public void onError(String arg0, String arg1, Exception e) {
				System.out.println("Pusher connection error: " + e.getMessage());
				
			}
			
			@Override
			public void onConnectionStateChange(ConnectionStateChange csc) {
				
			}
		}, ConnectionState.ALL);
		
		Channel channel = pusher.subscribe("event_update_channel");
		
		channel.bind("event_updated", new SubscriptionEventListener() {
			
			@Override
			public void onEvent(String channel, String event, String data) {
				System.out.println("Pusher event received with data: " + data);
				System.out.println("Updating Automation Schedules...");
				sManager.updateSchedule();
			}
		});
		
		System.out.println("Pusher setup complete.");
	}
}
