package ControlHub;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import almonds.*;

public class ParseServerCommunicator{
	
	public ParseServerCommunicator()
	{
		setupParse();
	}

	void setupParse()
	{
		String appID = "k9UH8HfBh2kObyiosc1Pu99Sf3L3zEk7mgGLvo3S";
		String restAPIKey = "XcgCLE4DZZibciKQ9UBtgATQzXmuH9uIwdo16zUC";
		Parse.initialize(appID, restAPIKey);
	}
	
	public <T>void WriteValue(String table, String column, T value)
	{
		ParseObject object = new ParseObject(table);
		object.put(column, value);
		object.saveInBackground(new SaveCallback() {
			public void done(ParseException e) {
				//Add a log you lazy bastard
			}
		});
	}
	
	public void WriteSerialPort(List<String> portNames, String table, String controlHubId)
	{
		System.out.println("Writing Ports");
		for(String name: portNames)
		{
			if(!checkExist(name,table,controlHubId))
			{
				System.out.println("Writing " + name + " to " + table);
				ParseObject object = new ParseObject(table);
				object.put("PortName", name);
				object.put("Name", "new");
				object.put("ControlHubId", controlHubId); //has to be parseobject not an id
				object.saveInBackground(new SaveCallback() {
					public void done(ParseException e) {
						if(e != null) System.out.println(e.toString());
					}
				});
			}
		}	
	}
	
	private boolean checkExist(String portName, String table, String controlHubId)
	{
		try {
			ParseQuery q = new ParseQuery(table);
			q.whereEqualTo("PortName", portName);
			q.whereEqualTo("ControlHubId", controlHubId);
			List<ParseObject> objectList = q.find();
			if(objectList.size() >= 1) return true;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public void WriteMonitorData(MonitorData mData)
	{
		ParseObject object = new ParseObject("MonitorData");
		
		float humdity = (Float) mData.get("humidity");
		float fahrenheit = (Float) mData.get("fahrenheit");
		int LDR = (Integer) mData.get("LDR");
		int wls = (Integer) mData.get("waterLevel");
		String monitorId = getMonitorId((String) mData.get("PortName"));
		
		System.out.println("Got objects...");
		
		object.put("fahrenheit", fahrenheit);
		object.put("humidity", humdity);
		object.put("LDR", LDR);
		object.put("waterLevel", wls);
		object.put("MonitorId", monitorId);
		
		System.out.println("Pushing data to Parse...");
		 
		object.saveInBackground(new SaveCallback() {
			public void done(ParseException e) {
				//Add a log you lazy bastard
			}
		});
	}
	
	public void WritePumpLightState(int state, String type)
	{
		ParseObject p = new ParseObject("AutomationState");
		p.put("Type", type);
		p.put("State", state);
		p.saveInBackground();
	}
	
	private String getMonitorId(String portName)
	{
		try
		{
			ParseQuery q = new ParseQuery("Monitor");
			q.whereEqualTo("PortName", portName);
			List<ParseObject> objectList = q.find();
			if(objectList.size() >=1) 
			{
				ParseObject obj = objectList.get(0);
				String id = obj.getObjectId();
				return id;
			}
			
		}
		catch(ParseException e)
		{
			System.out.println(e.toString());
		}
		return "";
	}
	
	public List<ParseObject> QueryEvents(String scheduleId)
	{
		List<ParseObject> objectList = new ArrayList<ParseObject>();
		ParseQuery query = new ParseQuery("Event");
		query.whereEqualTo("ScheduleId", scheduleId);
		query.orderByAscending("FirstOccurrence");
		
		try{
			objectList = query.find();
		}
		catch(ParseException e)
		{
			e.printStackTrace();
		}
		
		return objectList;
	}
	
	public List<ParseObject> QuerySchedule(int scheduleType)
	{
		List<ParseObject> objectList = new ArrayList<ParseObject>();
		ParseQuery query = new ParseQuery("Schedule");
		query.whereEqualTo("Type", scheduleType);
		
		try{
			objectList = query.find();
		}
		catch(ParseException e)
		{
			e.printStackTrace();
		}
		
		return objectList;
	}
	
	public Object QueryValue(String table, String column)
	{
		Object response = null;
		ParseQuery query = new ParseQuery(table);
		query.orderByDescending("updatedAt");
		
		try {
			List<ParseObject> objectList = query.find();
			ParseObject object = objectList.get(0);
			response = object.get(column);
		} 
		catch (ParseException e) 
		{
			//Add logging 
			e.printStackTrace();
		}
		return response;
	}
}
